﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag;
       double eingeworfeneMuenze;
       double rueckgeld;
      
       double eingezahlterGesamtbetrag;
      
       

       //Anzahl der Tickets
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
   

       // Geldeinwurf
       // -----------
       
       rueckgeld = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
       
      
       // Fahrscheinausgabe
       // -----------------
       
       fahrkartenAusgeben();


       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rueckgeld > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rueckgeld + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rueckgeld >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
        	  rueckgeld -= 2.0;
           }
           while(rueckgeld >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
        	  rueckgeld -= 1.0;
           }
           while(rueckgeld >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
        	  rueckgeld -= 0.5;
           }
           while(rueckgeld >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
        	  rueckgeld -= 0.2;
           }
           while(rueckgeld >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
        	  rueckgeld -= 0.1;
           }
           while(rueckgeld >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
        	  rueckgeld -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
   
    
    //Methoden
    
    public static void fahrkartenAusgeben() {
        
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
        }
    
   
    
    public static double fahrkartenbestellungErfassen(Scanner tastatur) {
 	   double anzahl; 
 	   double zuZahlenderBetrag;
 	   double eingezahlterGesamtbetrag;
 	   
 	   System.out.print("Wie viele Tickets wollen sie Kaufen?: ");
        anzahl = tastatur.nextDouble();
        
      
        System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        eingezahlterGesamtbetrag = anzahl * zuZahlenderBetrag;
        
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenzen;
        double rueckgeld;
     
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
              
        }
        
        rueckgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        
        return rueckgeld;
        
         }

    
}


