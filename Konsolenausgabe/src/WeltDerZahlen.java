/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 27.10.21
  * @author << Felix W�rzberg >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
        long anzahlSterne = 150000000000l;
    
    // Wie viele Einwohner hat Berlin?
       int bewohnerBerlin = 3766082 ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
      short alterTage = 5961;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm =  190000; 
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
     int  flaecheGroessteLand = 17100000;
    
    // Wie gro� ist das kleinste Land der Erde?
    
       double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println(" " );
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println(" " );
    
    System.out.println("Einwohnerzahl Berlin 2021: " + bewohnerBerlin);
    
    System.out.println(" " );
    
    System.out.println("Mein Alter in Tagen: " + alterTage);
    
    System.out.println(" " );
    
    System.out.println("Das schwerste tier der Welt in KG: " + gewichtKilogramm);
    
    System.out.println(" " );
    
    System.out.println("Fl�che des gr��ten Landes der Welt in km�: " + flaecheGroessteLand);
    
    System.out.println(" " );
    
    System.out.println("Fl�che des kleinsten Landes der Welt in km�: " + flaecheKleinsteLand);
    
    System.out.println(" " );
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
