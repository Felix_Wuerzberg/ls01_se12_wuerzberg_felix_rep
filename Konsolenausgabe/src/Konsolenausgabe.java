
public class Konsolenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
	 System.out.print("Das ist ein \"Beispielsatz\". Dies ist der \nzweite\n \"Beispielsatz\""); 
	 System.out.println("  ");
	 

	 //	 Der Unterschied zwischen print() und println(line)() ist, dass Println einen Zeilenumbruch ausf�hrt. Befehle die danach ausgef�hrt werden
     //	 sind immer darunter gelistet. Bei print ist alles in einer Zeile.
	 
	 
	 System.out.println("______________Aufgabe 2 (Form)____________________");
	 System.out.println("  ");
	 
	 System.out.println("      *");
	 System.out.println("     ***");
	 System.out.println("    *****");
	 System.out.println("   *******");
	 System.out.println("  *********");
	 System.out.println(" ***********");
	 System.out.println("**************");
	 System.out.println("     ***");
	 System.out.println("     ***");
	 System.out.println("     ***");
	 
	 
	 
	 System.out.println("______________Aufgabe 3 (Zahlen)____________________");
	 System.out.println("  ");
 
	 double KommaZahl1; 
	 
	 KommaZahl1 = 22.4234234;
	 
	 System.out.printf( "%.2f" , KommaZahl1);
	 System.out.println("  ");
	 
	 double KommaZahl2;
	 KommaZahl2 = 111.2222;
	 
	 System.out.printf( "%.2f" , KommaZahl2);
	 
	 System.out.printf( "%.2f" , KommaZahl1);
	 System.out.println("  ");
	 
	 double KommaZahl3;
	 KommaZahl3 = 4.0;
	 
	 System.out.printf( "%.2f" , KommaZahl3);
 System.out.println("  ");
	 
	 double KommaZahl4;
	 KommaZahl4 = 1000000.551;
	 
	 System.out.printf( "%.2f" , KommaZahl4);
 System.out.println("  ");
	 
	 double KommaZahl5;
	 KommaZahl5 = 97.34;
	 
	 System.out.printf( "%.2f \n" , KommaZahl5);

	 
	 System.out.println("______________Aufgabe 1 �bung 2____________________");
	 
	 String s = "* *";
	 
	 
	 System.out.printf( "\n%20s\n", s ); 
	 System.out.println("               *     *");
	 System.out.print("               *     *");
	 System.out.printf( "\n%20s\n", s ); 
	 System.out.println("");
	 
	 System.out.println("______________Aufgabe 2 �bung 2____________________");
	 System.out.println("   ");
	 
	 String b = "0!";
	 String c = "1!";
	 String d = "2!";
	 String e = "3!";
	 String f = "4!";
	 String g = "5!";
	 String h = " ";
	 String j = "1";
	 String k = "1";
	 String l = "1 * 2";
	 String m = "2";
	 String n = "1 * 2 * 3";
	 String o = "6";
	 String p = "1 * 2 * 3 * 4";
	 String q = "24";
	 String r = "1 * 2 * 3 *4 * 5";
	 // String s �bersprungen weil schon benutzt
	 String t = "120";
	 
	 System.out.printf( "%-5s = %-19s = %4s \n", b , h , j);
	 System.out.printf( "%-5s = %-19s = %4s \n"  , c, k, j );
	 System.out.printf( "%-5s = %-19s = %4s \n", d, l , m );
	 System.out.printf( "%-5s = %-19s = %4s \n", e , n , o );
	 System.out.printf( "%-5s = %-19s = %4s \n", f , p , q );
	 System.out.printf( "%-5s = %-19s = %4s \n", g , r , t );
 
	}
	
}
